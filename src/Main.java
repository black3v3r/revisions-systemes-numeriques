import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println(41 & 75);    // 9
        System.out.println(41 | 75);    // 107
        System.out.println(3 << 5);     // 96
        System.out.println(34 >> 3);    // 4
        System.out.println(~41);        // -42
        System.out.println(41 ^ 16);    // 57

        /* A tester avec n'importe quel nombre, renvoie 0 s'il est impair, 1 s'il est pair */
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        scanner.nextLine();
        System.out.println(1 ^ (a & 1)); // 1 ^ (a & 1)

        a = 0x55;                        // 0x** représente un nombre héxadécimal ici 0x55=01010101=85
        int b = 0x08;
        int c = a & b;
        System.out.println(c);           // 0

        a = 0x55;
        b = 0xFE;
        c = a | b;
        System.out.println(c);          // 255

        a = 0754;                       // On met 0 devant, pour utiliser la représentation octale, chaque utilisateur est donc représenté sur 3 bit 111=>7 101=>5 100=>4
        System.out.println(a | (1 << 4));// 508 décimal, soit 774 octal, ce qui correspond au résultat attendu

        for (int i = 'A'; i <= (int) 'Z'; i++) {
            System.out.println((char) i);
        }

        System.out.println((int) '9');  // 57
    }
}
